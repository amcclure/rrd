#!/usr/bin/perl -w
#
# HTML page generator for rrd stats
#
# Copyright (C) 2021-2022 Anton McClure <anton@antonmcclure.org>
#
# This file is free software: you may copy, redistribute and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, see https://gnu.org/licenses or write to:
#   Free Software Foundation, Inc.
#   51 Franklin Street, Fifth Floor
#   Boston, MA 02110-1301
#   USA
#
# This file incorporates work covered by the following copyright and 
# permission notice:
#
# Copyright (C) 2003, 2005, 2006, 2008, 2011, 2017  Christian Garbs <mitch@cgarbs.de>
# Licensed under GNU GPL v3 or later.
#
# This file is part of my rrd scripts (https://github.com/mmitch/rrd).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use warnings;
use Socket;
use Sys::Hostname;

# parse configuration file
my %conf;
eval(`cat ~/.rrd-conf.pl`);
die $@ if $@;

# set variables
my $host     = hostname();
my $path     = $conf{OUTPATH};
my @rrd      = @{$conf{MAKEHTML_MODULES}};
my @time     = qw(hour day week month year decade);
my $title    = "RRD statistics on $host";
my $progname = "Anton's RRD Scripts";
my $proghome = "./";
my $proglink = "http://www.antonmcclure.org/software/rrd";
my $progagpl = "$proglink/agpl-3.0.txt";
my $progdl = "$proglink/rrd-current.tar.gz";

# other files to include
my @MORE     = qw(make_html.gz Makefile.gz sample.conf.gz);

sub insert_links($);


foreach my $time (@time) {
    my $filepage = "$path/$time.html";
    print "generating `$filepage'\n";
    open HTML, '>', $filepage or die "can't open `$filepage': $!";

    print HTML "<html><head><title>$title (last $time)</title><meta http-equiv=\"refresh\" content=\"150; URL=$time.html\"></head><body>";

    print HTML "[ <a href=\"$proghome\">server home</a> ] ";
    insert_links($time);
    print HTML " [ <a href=\"$proglink/\">project home</a> ]";

    print HTML "<hr>";

    print HTML "<h1>$title</h1><h2>Last $time</h2>";

    foreach my $rrd (@rrd) {
	print HTML "<img style=\"margin:8px\" border=1 src=\"$rrd-$time.png\" alt=\"$rrd (last $time)\" align=\"top\">";
    }

    print HTML "<hr><address>";
    print HTML "Powered by <a href=\"$proglink/\">$progname</a>. Licensed under the <a href=\"$progagpl\">GNU Affero General Public Licence</a>, version 3 or any later version. You may download the version running here from the <a href=\"$progdl\">project's web site</a>.";
    print HTML "</address></body></html>";

    close HTML or  die "can't close `$filepage': $!";
}

my $fileindex = "$path/index.html";
print "generating `$fileindex'\n";
open HTML, '>', $fileindex or die "can't open `$fileindex': $!";

print HTML "<html><head><title>$title</title></head><body>";

print HTML "<h1>$title</h1>";

print HTML "<ul>";

foreach my $time (@time) {
    print HTML "<li><a href=\"$time.html\">$time</a>";
}

print HTML "</ul>";

print HTML "<hr><address>";
print HTML "Powered by <a href=\"$proglink/\">$progname</a>. Licensed under the <a href=\"$progagpl\">GNU Affero General Public Licence</a>, version 3 or any later version. You may download the version running here from the <a href=\"$progdl\">project's web site</a>.";
print HTML "</address></body></html>";

close HTML or  die "can't close `$fileindex': $!";

sub insert_links($)
{
    my $time = shift;
    my $bar = 0;
    print HTML "[";
    foreach my $linktime (@time) {
	if ($bar) {
	    print HTML "|";
	} else {
	    $bar = 1;
	}
	if ($linktime eq $time) {
	    print HTML " $linktime ";
	} else {
	    print HTML " <a href=\"$linktime.html\">$linktime</a> ";
	}
    }
    print HTML "]";
}
