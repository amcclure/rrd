Anton's RRD scripts

My RRD statistic scripts, based on mitch's rrd scripts. The project is available
for download at <http://www.antonmcclure.org/software/rrd/rrd-current.tar.gz>.

The original git repo is available at <https://github.com/mmitch/rrd>, and this
project's git repo us currently at <https://github.com/antonmcclure/rrd>.

================================================================================

This is a collection of scripts that draw some nice graphs with
information about your system: cpu load, network load, free disk space
and so on.

Various scripts are included.  Most will be useful for everyone, but
some are very special for my personal needs.  You probably won't find
them useful.

The scripts are not designed to be deployed on a server farm with
hundreds of systems nor do they provide alerts if something goes
wrong - they are completely passive.  There are various other tools
for these kind of scenarios (but if you do look for a small and simple
solution for some local nomitoring and alerting, have a look at
<https://github.com/mmitch/nomd>).

Dependencies
------------

For starters, you will need the `rrdtool` package, `bash` (any
`sh`-style shell might work, but currently it says `#!/usr/bin/bash`),
the `lockfile` program, `Perl` and the `RRDs` Perl module (not
available separately on CPAN, comes with the `rrdtool` package;
available as `librrds-perl` on Debian/Ubuntu).

Some modules will need other things as well.  You'll see it when
something breaks :-)

Setup
-----

See <http://www.antonmcclure.org/rrd-install.html> for instructions.

License
-------

Copyright (C) 2021-2022 Anton McClure <anton@antonmcclure.org>

This file is free software: you may copy, redistribute and/or modify it 
under the terms of the GNU Affero General Public License as published by 
the Free Software Foundation, either version 3 of the License, or (at 
your option) any later version.

This file is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public 
License for more details.

You should have received a copy of the GNU Affero General Public License 
along with this program; if not, see https://gnu.org/licenses or write to:
  Free Software Foundation, Inc.
  51 Franklin Street, Fifth Floor
  Boston, MA 02110-1301
  USA

This file incorporates work covered by the following copyright and 
permission notice:

Copyright (C) 2003-2009, 2011, 2013, 2015-2018 Christian Garbs <mitch@cgarbs.de>
Licensed under GNU GPL v3 or later.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
