#!/bin/bash
#
# spreads all desired scripts over a 5 minute window
#
# Copyright (C) 2021 Anton McClure <anton@antonmcclure.org>
#
# This file is free software: you may copy, redistribute and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, see https://gnu.org/licenses or write to:
#   Free Software Foundation, Inc.
#   51 Franklin Street, Fifth Floor
#   Boston, MA 02110-1301
#   USA
#
# This file incorporates work covered by the following copyright and
# permission notice:
#
# Copyright (C) 2007, 2008, 2011, 2013, 2015-2019  Christian Garbs <mitch@cgarbs.de>
# Licensed under GNU GPL v3 or later.
#
# This file is part of my rrd scripts (https://github.com/mmitch/rrd).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# set this to your git checkout; either relative or absolute
SCRIPTPATH=/root/rrd

#
# set sleep time in seconds between calls
# total script runtime should not exceed 5 minutes!
#RRD_WAIT=${RRD_WAIT:-5}

#
# how many graphs should be rendered?
# 0 = dynamic (twice per hour = 1, otherwise = 5)
# 1 = [hour, day, week, year] every 5 minutes
# 2 =       [day, week, year] every 5 minutes
#  [...]
# 5 = none ==              [] every 5 minutes
DRAW_DETAILS=0

LANG=C
export LANG

#
# dynamic graph details depending on time
if [ $DRAW_DETAILS -eq 0 ] ; then
    printf -v MINUTE '%(%M)T' -1
    MINUTE=$(( ${MINUTE#0} % 30 ))
    if [ $MINUTE -lt 8 ] ; then
	DRAW_DETAILS=1
    else
	DRAW_DETAILS=8
    fi
fi

#
# translate graph details to seconds for easier implementation in the scripts
case $DRAW_DETAILS in
    1) DRAW_DETAILS=3600       # hour
       ;;
    2) DRAW_DETAILS=86400      # day
       ;;
    3) DRAW_DETAILS=604800     # week
       ;;
    4) DRAW_DETAILS=2629800    # month
       ;;
    5) DRAW_DETAILS=31536000   # year
       ;;
    6) DRAW_DETAILS=315400000  # decade
       ;;
    7) DRAW_DETAILS=315400001 # decade + 1 -> never
       ;;
esac

#
# There was more than I needed here.
#
# call the scripts and wait between calls to spread the load
###"$SCRIPTPATH"/network.pl $DRAW_DETAILS
###/bin/sleep 0
###"$SCRIPTPATH"/tunnels.pl $DRAW_DETAILS
###/bin/sleep 0
###"$SCRIPTPATH"/temperature.pl $DRAW_DETAILS 2> /dev/null
###
###/bin/sleep 0
###"$SCRIPTPATH"/memory.pl $DRAW_DETAILS
###/bin/sleep 0
###"$SCRIPTPATH"/load.pl $DRAW_DETAILS
###/bin/sleep 0
###"$SCRIPTPATH"/diskfree.pl $DRAW_DETAILS 2> /dev/null
###
####/bin/sleep 0
####"$SCRIPTPATH"/ups.pl $DRAW_DETAILS 2>&1 | grep -F -v 'Init SSL without certificate database'
###/bin/sleep 0
###"$SCRIPTPATH"/cpu.pl $DRAW_DETAILS
###/bin/sleep 0
###
###"$SCRIPTPATH"/io.pl $DRAW_DETAILS 2> /dev/null
###/bin/sleep 0
###"$SCRIPTPATH"/netstat.pl $DRAW_DETAILS
####/bin/sleep 0
####"$SCRIPTPATH"/unbound.pl $DRAW_DETAILS
####/bin/sleep 0
####"$SCRIPTPATH"/firewall.pl $DRAW_DETAILS
###
####/bin/sleep 0
####"$SCRIPTPATH"/connecttime.pl $DRAW_DETAILS
###/bin/sleep 0
###"$SCRIPTPATH"/bogofilter.pl $DRAW_DETAILS
###/bin/sleep 0
###"$SCRIPTPATH"/cpufreq.pl $DRAW_DETAILS
###
####/bin/sleep 0
####"$SCRIPTPATH"/roundtrip.pl $DRAW_DETAILS
###/bin/sleep 0
###"$SCRIPTPATH"/ntpd.pl $DRAW_DETAILS
###/bin/sleep 0
###"$SCRIPTPATH"/entropy.pl $DRAW_DETAILS

"$SCRIPTPATH"/network.pl $DRAW_DETAILS
#/bin/sleep 22
"$SCRIPTPATH"/tunnels.pl $DRAW_DETAILS
#/bin/sleep 22
"$SCRIPTPATH"/temperature.pl $DRAW_DETAILS 2> /dev/null
#/bin/sleep 22
"$SCRIPTPATH"/memory.pl $DRAW_DETAILS
#/bin/sleep 22
"$SCRIPTPATH"/load.pl $DRAW_DETAILS
#/bin/sleep 22
"$SCRIPTPATH"/diskfree.pl $DRAW_DETAILS 2> /dev/null
#/bin/sleep 22
"$SCRIPTPATH"/cpu.pl $DRAW_DETAILS
#/bin/sleep 22
"$SCRIPTPATH"/io.pl $DRAW_DETAILS 2> /dev/null
#/bin/sleep 22
"$SCRIPTPATH"/netstat.pl $DRAW_DETAILS
#/bin/sleep 22
"$SCRIPTPATH"/bogofilter.pl $DRAW_DETAILS
#/bin/sleep 22
"$SCRIPTPATH"/cpufreq.pl $DRAW_DETAILS
#/bin/sleep 22
"$SCRIPTPATH"/ntpd.pl $DRAW_DETAILS
#/bin/sleep 22
"$SCRIPTPATH"/entropy.pl $DRAW_DETAILS
